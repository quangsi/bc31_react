import React, { Component } from "react";
import shortid from "shortid";
import DanhSachNguoiDung from "./DanhSachNguoiDung";
import FormNguoiDung from "./FormNguoiDung";
import FormNguoiDungAntd from "./FormNguoiDungAntd";
import { initailUser } from "./utils_ex_QuanLyNguoiDung";

export default class Ex_QuanLyNguoiDung extends Component {
  state = {
    danhSachNguoiDung: [],
    userEdited: null,
  };

  handleThemNguoiDung = (user) => {
    // let cloneDanhSachNguoiDung = [...this.state.danhSachNguoiDung];
    // cloneDanhSachNguoiDung.push(user);

    // this.setState({ danhSachN guoiDung: cloneDanhSachNguoiDung });

    let newUser = { ...user, id: shortid.generate() };
    this.setState({
      danhSachNguoiDung: [...this.state.danhSachNguoiDung, newUser],
    });
  };

  handleRemoveUser = (id) => {
    // b1 tìm index từ id
    // b2 remove bằng splice
    // b3 setState

    // b1
    let index = this.state.danhSachNguoiDung.findIndex((user) => {
      return user.id == id;
    });
    // b2
    let cloneDanhSachNguoiDung = [...this.state.danhSachNguoiDung];
    if (index !== -1) {
      cloneDanhSachNguoiDung.splice(index, 1);
      // b3
      this.setState({
        danhSachNguoiDung: cloneDanhSachNguoiDung,
      });
    }
  };

  handleEditUser = (id) => {
    let index = this.state.danhSachNguoiDung.findIndex((user) => {
      return user.id == id;
    });
    if (index !== -1) {
      this.setState({
        userEdited: this.state.danhSachNguoiDung[index],
      });
    }
  };

  handleUpdateUser = (userEdited) => {
    console.log("userEdited: ", userEdited);

    let index = this.state.danhSachNguoiDung.findIndex((user) => {
      return user.id == userEdited.id;
    });

    if (index !== -1) {
      let cloneDanhSachNguoiDung = [...this.state.danhSachNguoiDung];
      cloneDanhSachNguoiDung[index] = userEdited;

      this.setState({
        danhSachNguoiDung: cloneDanhSachNguoiDung,
        userEdited: null,
      });
    }
  };

  render() {
    return (
      <div className="container py-5">
        <FormNguoiDung
          handleUpdateUser={this.handleUpdateUser}
          userEdited={this.state.userEdited}
          handleThemNguoiDung={this.handleThemNguoiDung}
        />
        {/* <FormNguoiDungAntd /> */}
        <DanhSachNguoiDung
          handleEditUser={this.handleEditUser}
          handleRemoveUser={this.handleRemoveUser}
          danhSachNguoiDung={this.state.danhSachNguoiDung}
        />
      </div>
    );
  }
}
