import React, { Component } from "react";
import { version } from "react-dom";
import { initailUser } from "./utils_ex_QuanLyNguoiDung";

/**
 *
 * hoTen
 * taiKhoan
 * matKhau
 * email
 *
 */

let alice = {
  name: "alice",
  age: 2,
};
alice.name = "bob";
alice["name"] = "Bob";

let key = "name";
alice[key] = 5;

export default class FormNguoiDung extends Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
    this.formRef = React.createRef();
  }
  state = {
    user: initailUser,
    isUserEdited: false,
  };
  // reset()

  componentDidMount() {
    this.inputRef.current.focus();
  }

  handleChangeUser = (event) => {
    //
    //

    let value = event.target.value;
    let key = event.target.name;

    let cloneUser = { ...this.state.user, [key]: value };
    // cloneUser.hoTen = value;

    this.setState({ user: cloneUser });
  };

  static getDerivedStateFromProps(props, state) {
    if (!state.user.hoTen && props.userEdited && !state.isUserEdited) {
      return { user: props.userEdited, isUserEdited: true };
      // ~ tương tự setState
    }
  }
  // componentWillReceiveProps
  render() {
    return (
      <div>
        <form ref={this.formRef}>
          <div className="form-group">
            <input
              ref={this.inputRef}
              onChange={(e) => {
                this.handleChangeUser(e);
              }}
              value={this.state.user.hoTen}
              type="text"
              className="form-control"
              name="hoTen"
              placeholder="Tên"
            />
          </div>
          <div className="form-group">
            <input
              onChange={(e) => {
                this.handleChangeUser(e);
              }}
              value={this.state.user.taiKhoan}
              type="text"
              className="form-control"
              name="taiKhoan"
              placeholder="Tài khoản"
            />
          </div>
          <div className="form-group">
            <input
              onChange={(e) => {
                this.handleChangeUser(e);
              }}
              value={this.state.user.matKhau}
              type="text"
              className="form-control"
              name="matKhau"
              placeholder="Mật khẩu"
            />
          </div>
          <div className="form-group">
            <input
              onChange={(e) => {
                this.handleChangeUser(e);
              }}
              value={this.state.user.email}
              type="text"
              className="form-control"
              name="email"
              placeholder="Email"
            />
          </div>

          {this.props.userEdited ? (
            <button
              type="button"
              onClick={() => {
                this.setState({
                  user: initailUser,
                  isUserEdited: false,
                });
                this.props.handleUpdateUser(this.state.user);
              }}
              className="btn btn-primary"
            >
              Cập nhật
            </button>
          ) : (
            <button
              type="button"
              onClick={() => {
                this.setState({
                  user: initailUser,
                });
                this.props.handleThemNguoiDung(this.state.user);
              }}
              className="btn btn-primary"
            >
              Thêm người dùng
            </button>
          )}
        </form>
      </div>
    );
  }
}
