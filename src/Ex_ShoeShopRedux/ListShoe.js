import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

class ListShoe extends Component {
  renderShoes = () => {
    return this.props.shoeArr.map((item) => {
      return (
        <ItemShoe handleAddToCard={() => {}} shoeData={item} key={item.id} />
      );
    });
  };

  render() {
    return <div className="row">{this.renderShoes()}</div>;
  }
}
let mapStateToProps = (state) => {
  return {
    shoeArr: state.shoeShopeReducer.shoeArr,
  };
};

export default connect(mapStateToProps)(ListShoe);
