import { message } from "antd";
import axios from "axios";
import React, { Component } from "react";
import shortid from "shortid";
import DanhSachNguoiDung from "./DanhSachNguoiDung";
import FormNguoiDung from "./FormNguoiDung";
import FormNguoiDungAntd from "./FormNguoiDungAntd";
import { userService } from "./user.service";
import { initailUser } from "./utils_ex_QuanLyNguoiDung";
import ReactLoading from "react-loading";
export default class Ex_QuanLyNguoiDungAxios extends Component {
  fetchDanhSanhNguoiDung = () => {
    this.setState({ isLoading: true });

    userService
      .getListUser()
      .then((res) => {
        console.log(res);
        this.setState({
          // isLoading: false,
          danhSachNguoiDung: res.data,
        });
      })
      .catch((err) => {
        this.setState({
          // isLoading: false,
        });
        console.log(err);
      });
  };
  componentDidMount() {
    this.fetchDanhSanhNguoiDung();
    // async function fetchData() {
    //   let result = await userService.getListUser();
    //   console.log(result);
    //   this.setState({ danhSachNguoiDung: result.data });
    // }

    // fetchData();
  }

  state = {
    danhSachNguoiDung: [],
    userEdited: null,

    isLoading: false,
  };

  handleThemNguoiDung = (user) => {
    // let cloneDanhSachNguoiDung = [...this.state.danhSachNguoiDung];
    // cloneDanhSachNguoiDung.push(user);

    // this.setState({ danhSachN guoiDung: cloneDanhSachNguoiDung });

    let newUser = { ...user, id: shortid.generate() };
    this.setState({
      danhSachNguoiDung: [...this.state.danhSachNguoiDung, newUser],
    });
  };

  handleRemoveUser = (id) => {
    userService
      .deletedUser(id)
      .then((res) => {
        message.success("xoá thành công");
        this.fetchDanhSanhNguoiDung();
      })
      .catch((err) => {
        message.error("Đã có lỗi xảy ra");
      });
  };

  handleEditUser = (id) => {
    let index = this.state.danhSachNguoiDung.findIndex((user) => {
      return user.id == id;
    });
    if (index !== -1) {
      this.setState({
        userEdited: this.state.danhSachNguoiDung[index],
      });
    }
  };

  handleUpdateUser = (userEdited) => {
    console.log("userEdited: ", userEdited);

    let index = this.state.danhSachNguoiDung.findIndex((user) => {
      return user.id == userEdited.id;
    });

    if (index !== -1) {
      let cloneDanhSachNguoiDung = [...this.state.danhSachNguoiDung];
      cloneDanhSachNguoiDung[index] = userEdited;

      this.setState({
        danhSachNguoiDung: cloneDanhSachNguoiDung,
        userEdited: null,
      });
    }
  };

  render() {
    return (
      <div className="container py-5">
        {this.state.isLoading ? (
          <ReactLoading
            type={"bubbles"}
            color={"red"}
            height={667}
            width={375}
          />
        ) : (
          ""
        )}
        <FormNguoiDung
          handleUpdateUser={this.handleUpdateUser}
          userEdited={this.state.userEdited}
          handleThemNguoiDung={this.handleThemNguoiDung}
        />
        {/* <FormNguoiDungAntd /> */}
        <DanhSachNguoiDung
          handleEditUser={this.handleEditUser}
          handleRemoveUser={this.handleRemoveUser}
          danhSachNguoiDung={this.state.danhSachNguoiDung}
        />
      </div>
    );
  }
}
