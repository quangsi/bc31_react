import axios from "axios";

let https = axios.create({
  baseURL: "https://62db6cb3e56f6d82a77285da.mockapi.io",
});

export const userService = {
  getListUser: () => {
    let uri = "/users";
    return https.get(uri);
  },

  deletedUser: (id) => {
    let uri = `users/${id}`;
    return https.delete(uri);
  },
  postCreateUser: (user) => {
    let uri = "/users";
    return https.post(uri, user);
  },
  getDetailUser: (id) => {
    let uri = `users/${id}`;
    return https.get(uri);
  },
  putUpdateUser: (id, user) => {
    let uri = `users/${id}`;
    return https.put(uri, user);
  },
};
