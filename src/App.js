import logo from "./logo.svg";
import "antd/dist/antd.css";

import "./App.css";
import DemoClass from "./DemoComponent/DemoClass";
import DemoFunction from "./DemoComponent/DemoFunction";
import DemoStyle from "./DemoStyle/DemoStyle";
import DataBinding from "./DataBinding/DataBinding";
import ConditionalRedering from "./ConditionalRedering/ConditionalRedering";
import EventBinding from "./EventBinding/EventBinding";
import DemoState from "./DemoState/DemoState";
import DemoStateNumber from "./DemoStateNumber/DemoStateNumber";
import DemoProps from "./DemoProps/DemoProps";
import Ex_BaiTapChonXe from "./Ex_BaiTapChonXe/Ex_BaiTapChonXe";
import RenderWithMap from "./RenderWithMap/RenderWithMap";
import Ex_ShoeShop from "./Ex_ShoeShop/Ex_ShoeShop";
import Ex_Phones from "./Ex_Phones/Ex_Phones";
import DemoReduxMini from "./DemoReduxMini/DemoReduxMini";
import Ex_ShoeShopRedux from "./Ex_ShoeShopRedux/Ex_ShoeShopRedux";
import Ex_game_xuc_xac from "./Ex_game_xuc_xac/Ex_game_xuc_xac";
import Demo_LifeCycle from "./Demo_LifeCycle/Demo_LifeCycle";
import Ex_QuanLyNguoiDung from "./Ex_QuanLyNguoiDung/Ex_QuanLyNguoiDung";
import Ex_QuanLyNguoiDungAxios from "./Ex_QuanLyNguoiDungAxios/Ex_QuanLyNguoiDungAxios";
// import DemoClass from "./DemoComponent/DemoClass";
function App() {
  return (
    <div className="App">
      {/* <DemoClass></DemoClass>
      <DemoClass />
      <DemoFunction /> */}
      {/* <DemoStyle /> */}
      {/* <div className="demo_title">Hello</div> */}

      {/* <DataBinding /> */}
      {/* <ConditionalRedering /> */}
      {/* <EventBinding /> */}
      {/* <DemoState /> */}
      {/* <DemoStateNumber /> */}
      {/* <DemoProps /> */}
      {/* <Ex_BaiTapChonXe /> */}
      {/* <RenderWithMap /> */}
      {/* <Ex_ShoeShop /> */}
      {/* <Ex_Phones /> */}
      {/* <DemoReduxMini /> */}
      {/* <Ex_ShoeShopRedux /> */}
      {/* <Ex_game_xuc_xac /> */}
      {/* <Demo_LifeCycle /> */}
      {/* <Ex_QuanLyNguoiDung /> */}
      <Ex_QuanLyNguoiDungAxios />
    </div>
  );
}

export default App;

// npm i redux react-redux
// redux : state management library

// react-redux : thư viện kết nối react và redux
