import { GIAM_SO_LUONG, TANG_SO_LUONG } from "../constants/numberConstant";

export const giamSoLuongAction = (value) => {
  return {
    type: GIAM_SO_LUONG,
    payload: value,
  };
};
