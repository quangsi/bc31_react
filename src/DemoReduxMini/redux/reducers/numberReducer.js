import { GIAM_SO_LUONG, TANG_SO_LUONG } from "../constants/numberConstant";

let initialState = {
  giaTri: 10,
};
/**
 *
 * @param {*} state  : nơi quản lý state
 * @param {*} action : mô tả thông tin cho reducer xử lý
 * @returns
 */
export const numberReducer = (state = initialState, action) => {
  switch (action.type) {
    case TANG_SO_LUONG: {
      state.giaTri = state.giaTri + action.payload;
      return { ...state };
    }
    case GIAM_SO_LUONG: {
      state.giaTri -= action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};
