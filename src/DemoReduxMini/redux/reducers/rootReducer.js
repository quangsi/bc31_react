import { combineReducers } from "redux";
import { numberReducer } from "./numberReducer";

export let rootReducer_demoMiniRedux = combineReducers({
  number: numberReducer,
});
/**
 * key : quản lý state của reducer
 * value : tên của reducer
 */
