import React, { Component } from "react";
import { connect } from "react-redux";
import { giamSoLuongAction } from "./redux/action/numberAction";
import { TANG_SO_LUONG } from "./redux/constants/numberConstant";

class DemoReduxMini extends Component {
  render() {
    console.log(this.props);
    return (
      <div className="mt-5 pb-5">
        <button
          onClick={() => {
            this.props.tangSoLuong(5);
          }}
          className="btn btn-success"
        >
          Increase
        </button>
        <span className="text-success display-4 mx-5">{this.props.number}</span>
        <button
          onClick={() => {
            this.props.giamSoLuong(10);
          }}
          className="btn btn-success"
        >
          Decrease
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    number: state.number.giaTri,
  };
  /**
   * key : tên props mà component hiện tại sử dụng
   * value: giá trị trên store
   */
};

let mapDispatchToProps = (dispatch) => {
  return {
    tangSoLuong: (value) => {
      let action = {
        type: TANG_SO_LUONG,
        payload: value,
      };
      // action : mô tả thông tin cho reducer xử lý
      // dispatch: callback redux sẽ gửi vào , nhận action làm tham số
      dispatch(action);
    },
    giamSoLuong: (value) => {
      dispatch(giamSoLuongAction(value));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(DemoReduxMini);
