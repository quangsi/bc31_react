import React, { Component } from "react";

export default class ConditionalRedering extends Component {
    isLogin = true;

    renderContent = () => {
      if (this.isLogin) {
        return <button className="btn btn-success">Logout</button>;
      } else {
        return <button className="btn btn-warning">Login</button>;
      }
    };
    render() {
      return (
        <div className="container py-5">
          <div>{this.renderContent()}</div>

          <div> {this.isLogin ? "Đã đăng nhập" : "Chưa đăng nhập"} </div>
        </div>
      );
    }
}
