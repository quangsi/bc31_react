import React, { Component } from "react";
import { connect } from "react-redux";
import { PLAY_GAME } from "./redux/constant/xucXacConstants";

class KetQua extends Component {
  render() {
    let { handlePlayGame, luaChon, soLuotChoi, soBanThang } = this.props;
    return (
      <div className=" text-center pt-5 text-white display-4">
        {/* play button */}
        <button onClick={handlePlayGame} className="btn btn-success">
          <span className="display-4">Play game</span>
        </button>
        {/* lựa chọn */}

        <p className=" mt-3">Bạn chọn: {luaChon}</p>
        <p className=" mt-3">Số lượt chơi: {soLuotChoi}</p>
        <p className=" mt-3">Số bàn thằng: {soBanThang}</p>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handlePlayGame: () => {
      dispatch({
        type: PLAY_GAME,
      });
    },
  };
};
let mapStateToProps = (state) => {
  return {
    luaChon: state.xucXacReducer.luaChon,
    soLuotChoi: state.xucXacReducer.soLuotChoi,
    soBanThang: state.xucXacReducer.soBanThang,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(KetQua);
