import React, { Component } from "react";

import "./demoStyle.css";
import styles from "./demoStyle.module.css";
export default class DemoStyle extends Component {
  render() {
    return <div className={styles.demo_title}>DemoStyle</div>;
  }
}
