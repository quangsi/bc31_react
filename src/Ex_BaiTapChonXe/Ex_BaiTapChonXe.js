import React, { Component } from "react";

export default class Ex_BaiTapChoXe extends Component {
  state = {
    imgSrc: "./img_state_props/CarBasic/products/black-car.jpg",
  };

  handleChangeColor = (color) => {
    console.log("color: ", color);
    this.setState({
      imgSrc: `./img_state_props/CarBasic/products/${color}-car.jpg`,
    });
  };

  render() {
    return (
      <div className="container py-5">
        <div className="row">
          <div className="col-5">
            <img className="w-100" src={this.state.imgSrc} alt="" />
          </div>
          <div className="col-7">
            <button
              onClick={() => {
                this.handleChangeColor("black");
              }}
              className="btn btn-dark"
            >
              Black
            </button>
            <button
              onClick={() => {
                this.handleChangeColor("red");
              }}
              className="btn btn-danger"
            >
              Red
            </button>
            <button
              onClick={() => {
                this.handleChangeColor("silver");
              }}
              className="btn btn-secondary"
            >
              Silver
            </button>
          </div>
        </div>
      </div>
    );
  }
}
